// eslint-disable-next-line import/no-extraneous-dependencies
import { mount, shallowMount } from '@vue/test-utils';
import JThemeSelector from '@/components/JThemeSelector/JThemeSelector.vue';
import JThemes from '@/components/JThemeSelector/JThemes';

describe('JThemeSelector.vue', () => {
  it('should render all available theme names', () => {
    const wrapper = shallowMount(JThemeSelector);
    Object.keys(JThemes).forEach((themeName) => {
      expect(wrapper.text().toLowerCase()).toContain(themeName);
    });
  });

  it('should set the clicked theme as current theme', () => {
    const wrapper = mount(JThemeSelector);

    Object.keys(JThemes).forEach((themeName) => {
      expect(wrapper.text().toLowerCase()).toContain(themeName);
    });
  });
});
