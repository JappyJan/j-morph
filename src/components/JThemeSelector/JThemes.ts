const JThemes: {
  [key: string]: {
    background: string;
    borderLight: string;
    borderDark: string;
    fontColor: string;
    fontColorClicked: string;
  };
} = {
  purple: {
    background: 'rgb(30,30,60)',
    borderLight: 'rgb(70,70,110)',
    borderDark: 'rgb(0,0,0)',
    fontColor: 'rgb(255, 255, 255)',
    fontColorClicked: 'rgb(200,200,200)',
  },
  light: {
    background: 'rgb(220,220,220)',
    borderLight: 'rgb(255,255,255)',
    borderDark: 'rgb(190,190,190)',
    fontColor: 'rgb(50,50,50)',
    fontColorClicked: 'rgb(80,80,80)',
  },
  dark: {
    background: 'rgb(40,40,40)',
    borderLight: 'rgb(70,70,70)',
    borderDark: 'rgb(20,20,20)',
    fontColor: 'rgb(255, 255, 255)',
    fontColorClicked: 'rgb(200,200,200)',
  },
};

export default JThemes;
