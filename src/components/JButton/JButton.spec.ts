// eslint-disable-next-line import/no-extraneous-dependencies
import { shallowMount } from '@vue/test-utils';
import JButton from '@/components/JButton/JButton.vue';

describe('JButton.vue', () => {
  it('renders a button with text', () => {
    const text = 'Hello World';
    const wrapper = shallowMount(JButton, {
      slots: {
        default: text,
      },
    });
    expect(wrapper.text()).toMatch(text);
  });

  it('it emits a click event', () => {
    let clicked = false;
    const wrapper = shallowMount(JButton, {
      listeners: {
        click: () => { clicked = true; },
      },
    });
    wrapper.element.click();
    expect(clicked).toBeTruthy();
  });

  it('it does not emit a click event when disabled', () => {
    let clicked = false;
    const wrapper = shallowMount(JButton, {
      propsData: {
        disabled: true,
      },
      listeners: {
        click: () => { clicked = true; },
      },
    });
    wrapper.element.click();
    expect(clicked).toBeFalsy();
  });
});
