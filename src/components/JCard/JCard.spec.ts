// eslint-disable-next-line import/no-extraneous-dependencies
import { shallowMount } from '@vue/test-utils';
import JCard from '@/components/JCard/JCard.vue';

describe('JCard/JCard.vue', () => {
  it('renders an empty container', () => {
    const wrapper = shallowMount(JCard);
    expect(wrapper.text()).toMatch('');
  });

  it('renders anything inside default slot', () => {
    const text = 'Hello World';
    const wrapper = shallowMount(JCard, {
      slots: {
        default: text,
      },
    });
    expect(wrapper.text()).toMatch(text);
  });
});
