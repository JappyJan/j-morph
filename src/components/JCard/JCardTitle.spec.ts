// eslint-disable-next-line import/no-extraneous-dependencies
import { shallowMount } from '@vue/test-utils';
import JCardText from '@/components/JCard/JCardText.vue';

describe('JCard/JCardTitle.vue', () => {
  it('renders an empty container', () => {
    const wrapper = shallowMount(JCardText);
    expect(wrapper.text()).toMatch('');
  });

  it('renders anything inside default slot', () => {
    const text = 'Hello World';
    const wrapper = shallowMount(JCardText, {
      slots: {
        default: text,
      },
    });
    expect(wrapper.text()).toMatch(text);
  });
});
