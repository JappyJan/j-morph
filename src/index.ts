import Vue from 'vue';
import './styles/main.css';
import JCard from '@/components/JCard/JCard.vue';
import JButton from '@/components/JButton/JButton.vue';
import JThemeSelector from '@/components/JThemeSelector/JThemeSelector.vue';
import JSeperator from '@/components/JSeperator/JSeperator.vue';
import JLabel from '@/components/label/JLabel.vue';
import JCardText from '@/components/JCard/JCardText.vue';
import JCardTitle from '@/components/JCard/JCardTitle.vue';

const components = {
  JCard,
  JCardTitle,
  JCardText,
  JLabel,
  JSeperator,
  JThemeSelector,
  JButton,
};

Object.keys(components).forEach((componentName) => {
  Vue.component(componentName, (components as unknown as {[key: string]: Vue})[componentName]);
});

export default components;
